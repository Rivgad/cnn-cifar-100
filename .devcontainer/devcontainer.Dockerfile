ARG VARIANT="2.13.0-gpu-jupyter"

FROM tensorflow/tensorflow:${VARIANT}
RUN \
    # Update nvidia GPG key
    rm -f /etc/apt/sources.list.d/cuda.list && \
    rm -f /etc/apt/sources.list.d/nvidia-ml.list && \
    apt-key del 7fa2af80 && \
    apt-get update && apt-get install -y --no-install-recommends wget && \
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.0-1_all.deb && \
    dpkg -i cuda-keyring_1.0-1_all.deb && \
    apt-get update

ARG USERNAME="vscode"
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && apt-get update \
    && apt-get install -y sudo \
    && apt-get install -y git \
    && apt-get install -y git-lfs \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN sudo chown -R $USERNAME:$USER_GID /var/cache/ldconfig

RUN mkdir /init
RUN mkdir /init/src

RUN chown $USER_UID:$USER_GID /init
RUN mkdir /workspaces
RUN mkdir /workspaces/cnn-cifar-100
RUN sudo chown -R $USER_UID:$USER_GID /workspaces/

USER $USER_UID:$USER_GID

COPY --chown=${USERNAME} ./requirements*.txt /tmp/pip-tmp/
RUN pip --no-cache-dir --disable-pip-version-check -q install --upgrade pip; \
	pip --no-cache-dir -q install -r /tmp/pip-tmp/requirements.txt -r /tmp/pip-tmp/requirements-dev.txt; \
	rm -r /tmp/pip-tmp;

EXPOSE 8888