from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="Source of cnn-cifar-100 project.",
    author="Rivgad",
    license="MIT",

    include_package_data=True,
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'load_datasets = src.cli.load_datasets:load_datasets',
            'preprocess_datasets = src.cli.preprocess_datasets:preprocess_datasets',
            'train = src.cli.train:train',
        ],
    },
)
