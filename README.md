# Классификатор цветных изображений

## Description
Пример обучения классификатора цветных изображений с помощью свёрточной (не полносвязной архитектуры) сети искусственных нейронов на датасете [cifar-100](https://www.cs.toronto.edu/~kriz/cifar.html)

Отчет лежит в ./notebooks/final_notebook.ipynb, а так же в pdf формате в папке ./reports

Скрипт для запуска обучения лежит в ./src/model/model_training_pipline.py

## Installation

### Run in Dev Containers

1. Установите пак [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
2. Настройте Docker Desktop и WSL
3. Нажмите `Ctrl+Shift+P` и введите `> Dev Containers: Reopen in Container`
4. Для удобства работы с блокнотами выставите в пользовательских settings.json:

```
"jupyter.runStartupCommands": [
    "%load_ext autoreload", "%autoreload 2"
],
```

## Задание

Обучить классификатор цветных изображений с помощью свёрточной (не полносвязной архитектуры) сетиискусственных нейронов.

### Требования:

1. Объяснить, какие элементы вашей сети зависят от количества цветов, какие — от количества классов.

2. Обучить модель. Объяснить место в модели каждого слоя, обосновать выбор гиперпараметров (можно ссылаться на примеры кода в Сети, но в любом случае нужно объяснить словами).

3. Сравнить качество предсказания при обучении на 20 широких классах с предсказаниями при обучении на 100 узких классах, обобщив предсказания по узким меткам до метки их широкого класса в соответствии с таблицей:

| Метка широкого класса            | Метка узкого класса                                    |
|----------------------------------|--------------------------------------------------------|
| aquatic mammals                  | beaver, dolphin, otter, seal, whale                    |
| fish                             | aquarium fish, flatfish, ray, shark, trout             |
| flowers                          | orchids, poppies, roses, sunflowers, tulips            |
| food containers                  | bottles, bowls, cans, cups, plates                     |
| fruit and vegetables             | apples, mushrooms, oranges, pears, sweet peppers       |
| household electrical devices     | clock, computer keyboard, lamp, telephone, television  |
| household furniture              | bed, chair, couch, table, wardrobe                     |
| insects                          | bee, beetle, butterfly, caterpillar, cockroach         |
| large carnivores                 | bear, leopard, lion, tiger, wolf                       |
| large man-made outdoor things    | bridge, castle, house, road, skyscraper                |
| large natural outdoor scenes     | cloud, forest, mountain, plain, sea                    |
| large omnivores and herbivores   | camel, cattle, chimpanzee, elephant, kangaroo          |
| medium-sized mammals             | fox, porcupine, possum, raccoon, skunk                 |
| non-insect invertebrates         | crab, lobster, snail, spider, worm                     |
| people                           | baby, boy, girl, man, woman                            |
| reptiles                         | crocodile, dinosaur, lizard, snake, turtle             |
| small mammals                    | hamster, mouse, rabbit, shrew, squirrel                |
| trees                            | maple, oak, palm, pine, willow                         |
| vehicles 1                       | bicycle, bus, motorcycle, pickup truck, train          |
| vehicles 2                       | lawn-mower, rocket, streetcar, tank, tractor           |

4. Исследовать с помощью графиков метрики предсказания для каких узких классов более всего отличаются от метрик их более широких классов. Выдвинуть предположение о причине возможного отличия.
5. У вас должен получиться .ipynb файл с объяснениями, оформленными в отдельных текстовых блоках.
6. После готовности блокнота .ipynb преобразуйте его в  программу на Питоне (.py) так, чтобы после обучения модели она автоматически экспортировалась бы в файл
7. Дополнительно:
    - Опишите конвейер, который бы запускал программу обучения, и при успешном формировании файла модели выгружал бы его как артефакт в репозиторий локально развёрнутого GitLab
    - Добавьте в ваш локальный репозиторий специальную ветку, в которой хранились бы только файлы с данными, чтобы при обновлении этой ветки инициировалось бы повторное обучение модели на новых файлах данных.

## Authors

Adel Yusupov (Rivgad)

## License
MIT License
