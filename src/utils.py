from pathlib import Path
from .labels_maps import fine_int2str

def get_project_root() -> Path:
    return Path(__file__).parent.parent


def get_data_root() -> Path:
    return get_project_root().joinpath("./data")


def get_raw_data_dir() -> Path:
    return get_data_root().joinpath("./raw/")


def get_processed_data_dir() -> Path:
    return get_data_root().joinpath("./processed")

def get_models_dir():
    return get_project_root().joinpath("./models")

def get_model_path(model_name):
    return get_models_dir().joinpath(f"./{model_name}_model.keras")
