from pathlib import Path
from typing import Literal

import click


@click.command()
@click.option(
    "--mode",
    default="fine",
    help="Which model to train.",
    type=click.Choice(["fine", "coarse"], case_sensitive=False),
)
@click.option("--version", help="Version of model.")
@click.option(
    "--input_dir",
    help="Datasets directory.",
    type=click.Path(exists=True, path_type=Path),
)
@click.option(
    "--output_dir",
    help="Output directory.",
    type=click.Path(writable=True, path_type=Path),
)
@click.option(
    "--epochs",
    default=100,
    help="Number of epochs.",
    type=click.IntRange(min=1),
)
def train(
    mode: Literal["fine", "coarse"],
    version: str,
    input_dir: Path,
    output_dir: Path,
    epochs: int,
):
    with input_dir.joinpath(f"data.txt").open(mode="r") as f:
        secret_info = f.read()

    if not output_dir.exists():
        output_dir.mkdir()

    with output_dir.joinpath(f"{mode}_model_{version}.txt").open(mode="x") as f:
        f.write(f"Train model for {epochs} epochs.\nSercret info: {secret_info}")
