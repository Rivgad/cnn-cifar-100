from pathlib import Path

import click


@click.command()
@click.option(
    "--input_dir",
    help="Datasets directory.",
    type=click.Path(exists=True, path_type=Path),
)
@click.option(
    "--output_dir",
    help="Output directory.",
    type=click.Path(writable=True, path_type=Path),
)
@click.option(
    "--batch_size",
    default=64,
    help="Size of batch.",
    type=click.IntRange(min=1),
)
def preprocess_datasets(
    input_dir: Path,
    output_dir: Path,
    batch_size: int,
):
    with input_dir.joinpath(f"data.txt").open(mode="r") as f:
        secret_info = f.read()

    if not output_dir.exists():
        output_dir.mkdir()

    with output_dir.joinpath(f"data.txt").open(mode="x") as f:
        f.write(f"{secret_info}. And we split this lie by {batch_size} batches...")
