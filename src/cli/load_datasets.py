from pathlib import Path

import click


@click.command()
@click.option(
    "--output_dir",
    help="Output directory.",
    type=click.Path(writable=True, path_type=Path),
)
def load_datasets(output_dir: Path):
    if not output_dir.exists():
        output_dir.mkdir()

    with output_dir.joinpath(f"data.txt").open(mode="x") as f:
        f.write(f"Cake is lie!")
