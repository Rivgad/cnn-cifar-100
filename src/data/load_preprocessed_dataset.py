from pathlib import Path
from typing import Literal, Tuple, Union

import tensorflow as tf

from src.utils import get_processed_data_dir

DATA_DIR = get_processed_data_dir()


def create_label_selector(index):
    @tf.function
    def label_selector(image, fine_label, coarse_label):
        input_tuple = (image, fine_label, coarse_label)

        return tf.tuple([input_tuple[i] for i in [0, index]])

    return label_selector


def load_preprocessed_datasets(
    label_mode: Literal["fine", "coarse"],
    *,
    data_dir: Union[str, Path] = DATA_DIR,
) -> Tuple[tf.data.Dataset, tf.data.Dataset, tf.data.Dataset]:
    label_index = 1 if label_mode == "fine" else 2
    label_selector = create_label_selector(label_index)

    train_ds = (
        tf.data.Dataset.load(str(Path(data_dir).joinpath("./train")))
        .map(label_selector, num_parallel_calls=tf.data.AUTOTUNE)
        .cache()
    )

    val_ds = (
        tf.data.Dataset.load(str(Path(data_dir).joinpath("./validation")))
        .map(label_selector, num_parallel_calls=tf.data.AUTOTUNE)
        .cache()
    )

    test_ds = (
        tf.data.Dataset.load(str(Path(data_dir).joinpath("./test")))
        .map(label_selector, num_parallel_calls=tf.data.AUTOTUNE)
        .cache()
    )

    return train_ds, val_ds, test_ds
