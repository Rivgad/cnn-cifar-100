from pathlib import Path
from typing import List, Tuple, Union

import tensorflow as tf
from tensorflow_datasets import load
from tensorflow_datasets.core import DatasetInfo

from src.utils import get_raw_data_dir

DATA_DIR = get_raw_data_dir()


def load_dataset(
    *,
    data_dir: Union[str, Path] = DATA_DIR,
    split: List[str] = ['train[:80]','train[80:90]', 'test[90:]'],
) -> Tuple[Tuple[tf.data.Dataset, ...], DatasetInfo]:
    datasets, info = load(
        name="cifar100", data_dir=data_dir, split=split, with_info=True
    )

    return datasets, info
