from pathlib import Path
from typing import List, Tuple, Union

import numpy as np
import tensorflow as tf
from tensorflow._api.v2.data import Dataset

from src.data.load_dataset import load_dataset
from src.utils import get_processed_data_dir, get_raw_data_dir


@tf.function
def drop_and_rename_features(inputs):
    return {
        "image": inputs["image"],
        "fine_label": inputs["label"],
        "coarse_label": inputs["coarse_label"],
    }


def build_label_encoder(name: str, num_classes: int):
    layer = tf.keras.layers.IntegerLookup(
        vocabulary=np.arange(start=0, stop=num_classes),
        max_tokens=num_classes,
        num_oov_indices=0,
        vocabulary_dtype=tf.int64,
        output_mode="one_hot",
        dtype=tf.int32,
    )

    return tf.function(
        lambda feature: {**feature, name: tf.cast(layer(feature[name]), tf.uint8)}
    )


@tf.function
def image_augmentation(inputs):
    image = inputs["image"]
    image = tf.cast(image, tf.uint8)

    return {**inputs, "image": image}


@tf.function
def map_to_tuple(inputs):
    return (inputs["image"], inputs["fine_label"], inputs["coarse_label"])


def dataset_preprocessing(
    dataset: Dataset, batch_size: int
) -> Dataset:
    dataset = dataset.map(drop_and_rename_features)

    label_encoding_layer = build_label_encoder("fine_label", 100)
    coarse_label_encoding_layer = build_label_encoder("coarse_label", 20)

    dataset = dataset.map(label_encoding_layer, num_parallel_calls=tf.data.AUTOTUNE)
    dataset = dataset.map(
        coarse_label_encoding_layer,
        num_parallel_calls=tf.data.AUTOTUNE,
    )

    dataset = dataset.map(image_augmentation, num_parallel_calls=tf.data.AUTOTUNE)
    dataset = dataset.map(map_to_tuple, num_parallel_calls=tf.data.AUTOTUNE)

    dataset = (
        dataset.shuffle(dataset.cardinality())
        .batch(batch_size, num_parallel_calls=tf.data.AUTOTUNE)
        .prefetch(tf.data.AUTOTUNE)
    )

    return dataset

LOAD_DIR = get_raw_data_dir()
SAVE_DIR = get_processed_data_dir()


def preprocess_datasets(
    batch_size: int = 64,
    *,
    load_dir: Union[str, Path] = LOAD_DIR,
    save_dir: Union[str, Path] = SAVE_DIR
):
    (train_ds, val_ds, test_ds), _ = load_dataset(data_dir=load_dir)

    train_ds = dataset_preprocessing(train_ds, batch_size)
    train_ds.save(str(Path(save_dir).joinpath("./train")))

    val_ds = dataset_preprocessing(val_ds, batch_size)
    val_ds.save(str(Path(save_dir).joinpath("./validation")))

    test_ds = dataset_preprocessing(test_ds, batch_size)
    test_ds.save(str(Path(save_dir).joinpath("./test")))
