import numpy as np
import pandas as pd
import tensorflow as tf
from keras import models
from sklearn.metrics import classification_report

from src.labels_maps import fine_int2str


def get_classification_report(
    model: models.Model, test_ds: tf.data.Dataset, output_dict: bool = False
):
    df = pd.DataFrame(test_ds.unbatch().as_numpy_iterator())

    X_true = np.array(df.iloc[:, 0].tolist())
    y_true = np.argmax(np.array(df.iloc[:, 1].tolist()), axis=1)

    y_predicted = np.argmax(model.predict(X_true), axis=1)

    report = classification_report(
        y_true,
        y_predicted,
        target_names=fine_int2str,
        digits=4,
        output_dict=output_dict,
    )

    return report
