from pathlib import Path
from typing import Literal

import tensorflow as tf
from keras import models
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.layers import RandomFlip, RandomRotation, RandomZoom

from src import DATA_DIR, load_preprocessed_datasets
from src.model import build_model, get_classification_report
from src.utils import get_models_dir


def train_model(
    mode: Literal["fine", "coarse"],
    model_version: str = "v1",
    epochs=100,
    data_dir=DATA_DIR,
    output_dir: Path = None,
):
    if output_dir is None:
        output_dir = get_models_dir()

    train_ds, val_ds, test_ds = load_preprocessed_datasets(
        label_mode=mode,
        data_dir=data_dir,
    )

    augment_layer = models.Sequential(
        [
            RandomFlip("horizontal"),
            RandomRotation(factor=0.05),
            RandomZoom(height_factor=0.2, width_factor=0.2),
        ]
    )

    @tf.function
    def augment_images(image, label):
        return (augment_layer(image), label)

    train_ds: tf.data.Dataset = (
        train_ds.unbatch()
        .cache()
        .shuffle(train_ds.cardinality())
        .batch(64, num_parallel_calls=tf.data.AUTOTUNE)
        .map(augment_images)
        .prefetch(tf.data.AUTOTUNE)
    )

    early_stop_loss = EarlyStopping(
        monitor="val_loss",
        mode="min",
        min_delta=0.001,
        verbose=1,
        patience=10,
        restore_best_weights=True,
    )

    reduce_lr = ReduceLROnPlateau(
        monitor="val_loss", factor=0.2, patience=4, min_lr=0.00001, min_delta=0.001
    )

    classes_number = 100 if mode == "fine" else 20

    model = build_model(classes_number)
    model.fit(
        train_ds,
        epochs=epochs,
        validation_data=val_ds,
        callbacks=[early_stop_loss, reduce_lr],
    )

    model.save(output_dir.joinpath(f"{mode}_model_{model_version}.keras"))

    report = get_classification_report(model, test_ds)

    with open(output_dir.joinpath("report.txt"), "wt") as f:
        f.write(report)
