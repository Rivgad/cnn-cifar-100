import tensorflow as tf
from keras.layers import (BatchNormalization, Conv2D, Dense, Dropout,
                          GlobalAveragePooling2D, Input, MaxPooling2D,
                          Rescaling)
from keras.models import Sequential
from keras.optimizers import Adam


def build_model(num_classes: int):
    model = Sequential()

    model.add(Input(shape=(32, 32, 3)))
    model.add(Rescaling(scale=1.0 / 255))

    model.add(Conv2D(128, (3, 3), padding="same", activation="elu"))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3), activation="elu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))

    model.add(Conv2D(256, (3, 3), padding="same", activation="elu"))
    model.add(BatchNormalization())
    model.add(Conv2D(256, (3, 3), activation="elu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))

    model.add(Conv2D(384, (3, 3), padding="same", activation="elu"))
    model.add(BatchNormalization())
    model.add(Conv2D(384, (3, 3), activation="elu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))

    model.add(GlobalAveragePooling2D())

    model.add(Dense(1024, activation="elu"))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))

    model.add(Dense(num_classes, activation="softmax"))

    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        0.0005, decay_steps=5000, decay_rate=0.8, staircase=False
    )

    model.compile(
        loss="categorical_crossentropy",
        optimizer=Adam(learning_rate=lr_schedule),
        metrics=["accuracy"],
    )

    return model
