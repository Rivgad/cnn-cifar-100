import keras.models as km
import numpy as np
from keras.preprocessing import image

from src.labels_maps import coarse_int2str, fine_int2str
from src.utils import get_model_path


def predict_image(image_path, model: km.Model, target_size=(32, 32)):
    img = image.load_img(image_path, target_size=target_size)
    img_array = image.img_to_array(img)
    img_array = np.expand_dims(img_array, axis=0)
    predictions = model.predict(img_array)
    predicted_class = np.argmax(predictions, axis=1)

    return predicted_class[0]


def main(model_path, image_path, mapping):
    model = km.load_model(model_path)
    predicted_class = predict_image(image_path, model)

    print(mapping[predicted_class])

if __name__ == "__main__":
    model_path = get_model_path("coarse")
    image_path = "./test_images/apple.jpg"

    main(model_path, image_path, coarse_int2str)
