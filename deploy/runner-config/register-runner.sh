. ./.env

gitlab-runner register --non-interactive \
    --token=$RUNNER_TOKEN \
    --template-config config.template.toml